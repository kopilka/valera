module.exports = {

  generate: {
    subFolders: false
  },
  /*
  ** Headers of the page
  */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' }
    ],
    script: [
      {src: 'js/uikit.min.js'},
      {src: 'js/uikit-icons-compmaster.min.js'},
    ],
  },


  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },



  css: [
      '@assets/css/uikit.compmaster.css',
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

